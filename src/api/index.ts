import axios from "axios";

const instance = axios.create({
  baseURL: "https://graphqlzero.almansi.me/api",
});

export const userAPI = {
  getProfile(id: number) {
    return instance.post("", {
      query: `
              query {
                user(id: ${id}) {
                    id
                    username
                    email
                    phone
                    website
                    company { name }
                    address { city }
                }
              }
            `,
    })
      .then((response) => response.data);
  },
};

export const feedAPI = {
  getAlbums(page: number) {
    return instance.post("", {
      query: `
              query {
                  albums (
                    options: {
                        paginate: {
                            page: ${page},
                            limit: 20,
                        }
                    }
                  )
                    {
                        data {
                            id
                            title
                            user { name }
                            photos {
                                data {
                                    id
                                    title
                                    thumbnailUrl
                                }
                            }
                        }
                    }
              }
            `,
    }).then((response) => response.data.data.albums.data);
  },
  deleteAlbum(albumId: string) {
    return instance.post("", {
      query: `
                mutation {
                    deleteAlbum(id: ${albumId})
                }`,
    }).then((response) => response.data.data);
  },
  addNewAlbum(title: string) {
    return instance.post("", {
      query: `
                mutation {
                    createAlbum(
                        input: { title: "${title}", userId: "1"}
                    ) {
                       id
                       title
                       user { name }
                       photos { data { thumbnailUrl } }
                   }
                }`,
    }).then((response) => response.data.data);
  },
  getPosts(page: number) {
    return instance.post("", {
      query: `
              query {
                  posts (
                    options: {
                        paginate: {
                            page: ${page},
                            limit: 20,
                        }
                    }
                  )
                    {
                        data {
                            id
                            title
                            body
                            user { name }
                        }
                    }
              }
            `,
    }).then((response) => response.data.data.posts.data);
  },
};
