import React from "react";
import {Text, TouchableOpacity, StyleSheet} from "react-native";
import colors from "../constants/Colors";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 56,
    backgroundColor: colors.red,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
  },
  text: {
    color: colors.white,
    fontSize: 14,
    fontFamily: "Ubuntu-Medium",
  },
});

type Props = {
    text: string,
    color?: string,
    onPress: () => void,
}
const Button = ({text, color = colors.red, onPress} : Props) => (
  <TouchableOpacity
    onPress={onPress}
    style={[styles.container, {backgroundColor: color}]}>
    <Text style={styles.text}>{text}</Text>
  </TouchableOpacity>
);

export default React.memo(Button);
