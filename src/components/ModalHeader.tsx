import React from "react";
import {
  Image, TouchableOpacity, View, StyleSheet,
} from "react-native";
import colors from "../constants/Colors";

const closeImage = require("../../assets/images/close.png");

const styles = StyleSheet.create({
  closeWrapper: {
    width: "100%",
    backgroundColor: colors.white,
    justifyContent: "center",
    padding: 22,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  closeIcon: {
    width: 12,
    height: 12,
  },
});

const ModalHeader = ({onPress}: {onPress: () => void}) => (
  <View style={styles.closeWrapper}>
    <TouchableOpacity
      hitSlop={{
        top: 30, left: 30, right: 30, bottom: 30,
      }}
      onPress={onPress}>
      <Image
        source={closeImage}
        style={styles.closeIcon} />
    </TouchableOpacity>
  </View>
);

export default React.memo(ModalHeader);
