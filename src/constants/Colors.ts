const colors = {
  red: "#F54747",
  blue: "#4E42E4",
  grey: "#AFAFAF",
  black: "#404040",
  linkBlue: "#1D63CB",
  white: "#FFFFFF",
  greyBorder: "#ececec",
};

export default colors;
