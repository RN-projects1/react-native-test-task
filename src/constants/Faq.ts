export type faqItemData = {
  id: number,
  question: string,
  answer: string,
}

export type faqItem = {
    id: number,
    topic: string,
    data: faqItemData [],
}

export const faqData = [
  {
    id: 0,
    topic: "Authorization Issues",
    data: [
      {
        id: 0,
        question: "What moderation means?",
        // eslint-disable-next-line max-len
        answer: "If you don’t have a social profile, make sure that your profile section is filled out with your best work. Not having a social profile will limit your campaign offers, but it’s still possible to get projects on Insense without an active social presence.\n"
                    + "If you don’t have a social profile, make sure that your.",
      },
      {
        id: 11,
        question: "How to register and start working with the app?",
        // eslint-disable-next-line max-len
        answer: "If you don’t have a social profile, make sure that your profile section is filled out with your best work. Not having a social profile will limit your campaign offers, but it’s still possible to get projects on Insense without an active social presence.\n"
                    + "If you don’t have a social profile, make sure that your.",
      },
      {
        id: 1,
        question: "Will receive offers from brands?",
        // eslint-disable-next-line max-len
        answer: "If you don’t have a social profile, make sure that your profile section is filled out with your best work. Not having a social profile will limit your campaign offers, but it’s still possible to get projects on Insense without an active social presence.\n"
                    + "If you don’t have a social profile, make sure that your.",
      },
    ],
  },
  {
    id: 1,
    topic: "How to register and start working with the app?",
    data: [
      {
        id: 3,
        question: "What moderation means(1)?",
        // eslint-disable-next-line max-len
        answer: "If you don’t have a social profile, make sure that your profile section is filled out with your best work. Not having a social profile will limit your campaign offers, but it’s still possible to get projects on Insense without an active social presence.\n"
                    + "If you don’t have a social profile, make sure that your.",
      },
      {
        id: 4,
        question: "How to register and start working with the app(1)?",
        // eslint-disable-next-line max-len
        answer: "If you don’t have a social profile, make sure that your profile section is filled out with your best work. Not having a social profile will limit your campaign offers, but it’s still possible to get projects on Insense without an active social presence.\n"
                    + "If you don’t have a social profile, make sure that your.",
      },
      {
        id: 5,
        question: "Will receive offers from brands(1)?",
        // eslint-disable-next-line max-len
        answer: "If you don’t have a social profile, make sure that your profile section is filled out with your best work. Not having a social profile will limit your campaign offers, but it’s still possible to get projects on Insense without an active social presence.\n"
                    + "If you don’t have a social profile, make sure that your.",
      },
    ],
  },
  {
    id: 2,
    topic: "Will receive offers from brands?",
    data: [
      {
        id: 6,
        question: "What moderation means(2)?",
        // eslint-disable-next-line max-len
        answer: "If you don’t have a social profile, make sure that your profile section is filled out with your best work. Not having a social profile will limit your campaign offers, but it’s still possible to get projects on Insense without an active social presence.\n"
                    + "If you don’t have a social profile, make sure that your.",
      },
      {
        id: 7,
        question: "How to register and start working with the app(2)?",
        // eslint-disable-next-line max-len
        answer: "If you don’t have a social profile, make sure that your profile section is filled out with your best work. Not having a social profile will limit your campaign offers, but it’s still possible to get projects on Insense without an active social presence.\n"
                    + "If you don’t have a social profile, make sure that your.",
      },
      {
        id: 8,
        question: "Will receive offers from brands(3)?",
        // eslint-disable-next-line max-len
        answer: "If you don’t have a social profile, make sure that your profile section is filled out with your best work. Not having a social profile will limit your campaign offers, but it’s still possible to get projects on Insense without an active social presence.\n"
                    + "If you don’t have a social profile, make sure that your.",
      },
    ],
  },
];
