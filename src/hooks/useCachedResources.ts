import * as Font from "expo-font";
import * as SplashScreen from "expo-splash-screen";
import * as React from "react";

const uRegular = require("../../assets/fonts/Ubuntu-Regular.ttf");
const uMedium = require("../../assets/fonts/Ubuntu-Medium.ttf");

export default function useCachedResources() {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHideAsync();

        // Load fonts
        await Font.loadAsync({
          "Ubuntu-Regular": uRegular,
          "Ubuntu-Medium": uMedium,
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        await SplashScreen.hideAsync();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  return isLoadingComplete;
}
