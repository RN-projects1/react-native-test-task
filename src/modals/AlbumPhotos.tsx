import React from "react";
import {
  View, Text, StyleSheet, SafeAreaView, Dimensions,
} from "react-native";
import Carousel, {AdditionalParallaxProps, ParallaxImage} from "react-native-snap-carousel";
import {CompositeNavigationProp} from "@react-navigation/native";
import {BottomTabNavigationProp} from "@react-navigation/bottom-tabs";
import {StackNavigationProp} from "@react-navigation/stack";
import colors from "../constants/Colors";
import ModalHeader from "../components/ModalHeader";
import {BottomTabParamList, FeedParamList} from "../navigation/types";
import {Album, AlbumPhoto} from "../redux/types";

const {width: screenWidth} = Dimensions.get("window");

type Props = {
    navigation: CompositeNavigationProp<
        BottomTabNavigationProp<BottomTabParamList, 'Feed'>,
        StackNavigationProp<FeedParamList>
        >;
    route: { params: { album: Album }}
}

type RenderPhoto = {
    item: {item: AlbumPhoto},
    parallaxProps: AdditionalParallaxProps | undefined
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    justifyContent: "flex-start",
    alignItems: "center",
  },
  carousel: {
    height: 340,
    marginTop: 84,
  },
  item: {
    width: screenWidth - 80,
  },
  imageContainer: {
    height: 320,
    borderRadius: 4,
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    resizeMode: "contain",
  },
  title: {
    marginTop: 32,
    fontSize: 16,
    color: colors.black,
    fontWeight: "500",
    width: "80%",

  },
});

const renderPhoto = ({item, parallaxProps} : RenderPhoto) => {
  return (
    <View style={styles.item}>
      <ParallaxImage
        source={{uri: item.item.thumbnailUrl}}
        containerStyle={styles.imageContainer}
        style={styles.image}
        parallaxFactor={0}
        {...parallaxProps} />
    </View>
  );
};

const AlbumPhotosModal = ({navigation, route}: Props) => {
  return (
    <SafeAreaView style={styles.container}>
      <ModalHeader onPress={() => navigation.goBack()} />
      {
        route?.params?.album?.photos?.data?.length ? (
          <React.Fragment>
            <View style={styles.carousel}>
              <Carousel
                sliderWidth={screenWidth}
                itemWidth={screenWidth - 80}
                data={route.params.album.photos.data}
                // @ts-ignore
                renderItem={(item: {item: AlbumPhoto}, parallaxProps?: (AdditionalParallaxProps | undefined)) => {
                  return renderPhoto({item, parallaxProps});
                }}
                hasParallaxImages={true} />
            </View>
            <Text style={styles.title}>{route.params.album.title}</Text>
          </React.Fragment>
        ) : (
          <Text style={styles.title}>{"В данном альбоме еще нет фото"}</Text>
        )
      }
    </SafeAreaView>
  );
};

export default React.memo(AlbumPhotosModal);
