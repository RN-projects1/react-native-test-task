import React from "react";
import {
  Modal,
  StyleSheet,
  Text,
  Image,
  View,
  TouchableWithoutFeedback,
} from "react-native";
import colors from "../constants/Colors";
import Button from "../components/Button";
import ModalHeader from "../components/ModalHeader";

type Props = {
    isVisible: boolean,
    onDelete: () => void,
    onClose: () => void,
}

const modalDeleteImage = require("../../assets/images/modalDelete.png");

const styles = StyleSheet.create({
  freeSpace: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  container: {
    height: 584,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  content: {
    height: 584,
    backgroundColor: colors.white,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  centerContent: {
    width: "100%",
    alignItems: "center",
    marginTop: 56,
  },
  deleteIcon: {
    width: 108,
    height: 127,
  },
  title: {
    marginTop: 30,
    fontFamily: "Ubuntu-Medium",
    fontSize: 20,
    color: colors.black,
  },
  desc: {
    marginTop: 16,
    fontFamily: "Ubuntu-Regular",
    fontSize: 14,
    color: colors.black,
  },
  deleteButton: {
    marginTop: 174,
    paddingHorizontal: 16,
  },
});

const DeleteModal = ({isVisible, onClose, onDelete}: Props) => {
  return (
    <View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={isVisible}>
        <TouchableWithoutFeedback
          onPress={onClose}>
          <View style={styles.freeSpace} />
        </TouchableWithoutFeedback>
        <View style={styles.container}>
          <View style={styles.content}>
            <ModalHeader onPress={onClose} />
            <View style={styles.centerContent}>
              <Image
                style={styles.deleteIcon}
                source={modalDeleteImage} />
              <Text style={styles.title}>Delete Album</Text>
              <Text style={styles.desc}>Album will be permanently deleted.</Text>
            </View>
            <View style={styles.deleteButton}>
              <Button
                text={"Delete"}
                onPress={onDelete} />
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default React.memo(DeleteModal);
