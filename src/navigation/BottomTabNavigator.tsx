import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {Image, ImageProps} from "react-native";
import {createStackNavigator} from "@react-navigation/stack";
import * as React from "react";

import FAQ from "../screens/faq";
import Feed from "../screens/feed";
import Profile from "../screens/profile";
import Question from "../screens/question";
import AddAlbum from "../screens/addAlbum/AddAlbum";
import AlbumPhotosModal from "../modals/AlbumPhotos";

import {
  BottomTabParamList,
  FAQParamList,
  FeedParamList,
  ProfileParamList,
} from "./types";
import colors from "../constants/Colors";

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

const feed = require("../../assets/images/tabbar/feed.png");
const faq = require("../../assets/images/tabbar/faq.png");
const profile = require("../../assets/images/tabbar/profile.png");

const images = {
  feed,
  faq,
  profile,
};

const defaultOptions = {
  headerBackTitleVisible: false,
  headerTintColor: colors.black,
  headerStyle: {
    shadowColor: "transparent",
  },
};

const tabBarOptions = {activeTintColor: colors.blue, showLabel: false, inactiveTintColor: colors.grey};

function TabBarIcon(props: { sourcePath: ImageProps; color: string }) {
  return (<Image
    source={props.sourcePath}
    resizeMode={"contain"}
    style={{width: 24, height: 24, tintColor: props.color}} />);
}

const FeedStack = createStackNavigator<FeedParamList>();
function FeedNavigator() {
  return (
    <FeedStack.Navigator
      screenOptions={defaultOptions}
      mode="modal">
      <FeedStack.Screen
        name="Feed"
        component={Feed}
        options={{headerTitle: "FEED"}} />
      <FeedStack.Screen
        name="AddAlbum"
        component={AddAlbum}
        options={{headerTitle: "ADD ALBUM"}} />
      <FeedStack.Screen
        name="AlbumPhotos"
        component={AlbumPhotosModal}
        options={{headerShown: false}} />
    </FeedStack.Navigator>
  );
}

const FAQStack = createStackNavigator<FAQParamList>();
function FAQNavigator() {
  return (
    <FAQStack.Navigator screenOptions={defaultOptions}>
      <FAQStack.Screen
        name="FAQ"
        component={FAQ}
        options={{headerTitle: "FAQ"}} />
      <FAQStack.Screen
        name="Question"
        component={Question} />
    </FAQStack.Navigator>
  );
}

const ProfileStack = createStackNavigator<ProfileParamList>();
function ProfileNavigator() {
  return (
    <ProfileStack.Navigator screenOptions={defaultOptions}>
      <ProfileStack.Screen
        name="Profile"
        component={Profile}
        options={{headerTitle: "PROFILE"}} />
    </ProfileStack.Navigator>
  );
}

export default function BottomTabNavigator() {
  return (
    <BottomTab.Navigator
      initialRouteName="Feed"
      tabBarOptions={tabBarOptions}>
      <BottomTab.Screen
        name="Feed"
        component={FeedNavigator}
        options={{
          tabBarIcon: ({color}) => (<TabBarIcon
            sourcePath={images.feed}
            color={color} />),
        }} />
      <BottomTab.Screen
        name="FAQ"
        component={FAQNavigator}
        options={{
          tabBarIcon: ({color}) => (<TabBarIcon
            sourcePath={images.faq}
            color={color} />),
        }} />
      <BottomTab.Screen
        name="Profile"
        component={ProfileNavigator}
        options={{
          tabBarIcon: ({color}) => (<TabBarIcon
            sourcePath={images.profile}
            color={color} />),
        }} />
    </BottomTab.Navigator>
  );
}
