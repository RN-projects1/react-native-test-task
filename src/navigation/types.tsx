import {faqItem} from "../constants/Faq";
import {Album} from "../redux/types";

export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  Feed: undefined;
  FAQ: undefined;
  Profile: undefined;
};

export type FeedParamList = {
  Feed: undefined;
  AddAlbum: undefined;
  AlbumPhotos: {
    album: Album
  };
};

export type FAQParamList = {
  FAQ: undefined;
  Question: {
    data: faqItem
  };
};
export type ProfileParamList = {
  Profile: undefined;
};
