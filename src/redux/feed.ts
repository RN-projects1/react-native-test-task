import {Alert} from "react-native";
import {feedAPI} from "../api";
import {Album, FeedReducerState, Post} from "./types";

const SET_ALBUMS = "SET_ALBUMS";
const START_LOADING = "START_LOADING";
const DELETE_ALBUM = "DELETE_ALBUM";
const ADD_ALBUM = "ADD_ALBUM";
const SET_POSTS = "SET_POSTS";
const SET_END_POSTS = "SET_END_POSTS";
const IS_END_ALBUMS = "IS_END_ALBUMS";

type ActionTypes = {
    type: string,
    payload: {
        albumId: string,
        albums: Album [],
        album: Album,
        posts: Post [],
    },
}

const initialState: FeedReducerState = {
  albums: [],
  isLoading: false,
  isEndAlbums: false,
  isEndPosts: false,
  albumPage: 1,
  postPage: 1,
  posts: [],
};

const feedReducer = (state = initialState, action: ActionTypes) => {
  switch (action.type) {
    case START_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case SET_ALBUMS:
      return {
        ...state,
        albums: [...state.albums, ...action.payload.albums],
        isLoading: false,
        albumPage: state.albumPage + 1,
      };
    case SET_POSTS:
      return {
        ...state,
        posts: [...state.posts, ...action.payload.posts],
        isLoading: false,
        postPage: state.postPage + 1,
      };
    case DELETE_ALBUM:
      return {
        ...state,
        albums: state.albums.filter((item) => item.id !== action.payload.albumId),
      };
    case SET_END_POSTS:
      return {
        ...state,
        isEndPosts: true,
      };
    case IS_END_ALBUMS:
      return {
        ...state,
        isEndAlbums: true,
      };
    case ADD_ALBUM:
      return {
        ...state,
        albums: [
          action.payload.album,
          ...state.albums,
        ],
      };
    default:
      return state;
  }
};

export const setAlbums = (albums: Album []) => {
  return {type: SET_ALBUMS, payload: {albums}};
};

const startLoading = () => {
  return {type: START_LOADING};
};

export const setEndAlbums = () => ({type: IS_END_ALBUMS});

export const getAlbums = () => async (
  dispatch: (arg0: { type: string; payload?: { albums: Album[]; }; }) => void,
  getState: () => { feed: { isEndAlbums: boolean; albumPage: number } },
) => {
  const {isEndAlbums, albumPage} = getState().feed;
  if (!isEndAlbums) {
    dispatch(startLoading());
    const data = await feedAPI.getAlbums(albumPage);
    if (data.length) {
      dispatch(setAlbums(data));
    } else {
      dispatch(setEndAlbums());
    }
  }
};

export const deleteAlbum = (albumId: string) => async (
  dispatch: (arg0: { type: string; payload: { albumId: string; }; }) => void,
) => {
  const data = await feedAPI.deleteAlbum(albumId);
  if (data.deleteAlbum) {
    dispatch({type: DELETE_ALBUM, payload: {albumId}});
  } else {
    Alert.alert("", "Упс, что-то пошло не так при удалении");
  }
};

export const addNewAlbum = ({title}: {title: string}) => async (
  dispatch: (arg0: { type: string; payload: { album: Album }; }) => void,
) => {
  const data = await feedAPI.addNewAlbum(title);
  if (data.createAlbum) {
    dispatch({type: ADD_ALBUM, payload: {album: data.createAlbum}});
  } else {
    Alert.alert("", "Упс, что-то пошло не так при создание альбома");
  }
};

export const setEndPosts = () => ({type: SET_END_POSTS});

export const getPosts = () => async (
  dispatch: (arg0: { type: string; payload?: { posts: any; }; }) => void,
  getState: () => { feed: { isEndPosts: boolean; postPage: number } },
) => {
  const {isEndPosts, postPage} = getState().feed;
  if (!isEndPosts) {
    dispatch(startLoading());
    const data = await feedAPI.getPosts(postPage);
    if (data.length) {
      dispatch({type: SET_POSTS, payload: {posts: data}});
    } else {
      dispatch(setEndPosts());
    }
  }
};
export default feedReducer;
