import {userAPI} from "../api";
import {UserProfile, UserProfileReducerState} from "./types";

const SET_USER_PROFILE = "SET_USER_PROFILE";

const initialState: UserProfileReducerState = {
  userProfile: null,
};

const profileReducer = (state = initialState, action: { type: string; payload: UserProfileReducerState }) => {
  switch (action.type) {
    case SET_USER_PROFILE:
      return {
        ...state,
        userProfile: action.payload.userProfile,
      };
    default:
      return state;
  }
};

export const setUserProfile = (userProfile: UserProfile) => {
  return {type: SET_USER_PROFILE, payload: {userProfile}};
};

export const getUserProfile = (id: number) => async (
  dispatch: (arg0: { type: string; payload: { userProfile: UserProfile; }; }) => void,
) => {
  const data = await userAPI.getProfile(id);
  dispatch(setUserProfile(data.data.user));
};

export default profileReducer;
