import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import profile from "./profle";
import feed from "./feed";

const reducers = combineReducers({
  profile,
  feed,
});

// eslint-disable-next-line no-undef
export type AppState = ReturnType<typeof reducers>;

const store = createStore(reducers, applyMiddleware(thunkMiddleware));

export default store;
