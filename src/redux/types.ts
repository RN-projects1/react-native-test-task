export type UserProfile = {
    id: string,
    username: string,
    email: string,
    phone: string,
    website: string,
    company: {
        name: string,
    },
    address: {
        city: string,
    }
} | null

export type UserProfileReducerState = {
    userProfile: UserProfile
}

export type Post = {
    id: string,
    title: string,
    body: string,
    user: { name: string}
}

export type AlbumPhoto = {
    id: string,
    title: string,
    thumbnailUrl: string,
}

export type Album = {
    id: string,
    title: string,
    user: {
        name: string,
    },
    photos: {
        data: AlbumPhoto []
    },
}

export type FeedReducerState = {
    albums: Album [],
    isLoading: boolean,
    isEndAlbums: boolean,
    isEndPosts: boolean,
    albumPage: number,
    postPage: number,
    posts: Post []
}
