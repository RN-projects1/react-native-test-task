import React, {useEffect, useState} from "react";
import {View, StyleSheet} from "react-native";
import {CompositeNavigationProp} from "@react-navigation/native";
import {BottomTabNavigationProp} from "@react-navigation/bottom-tabs";
import {StackNavigationProp} from "@react-navigation/stack";
import {useDispatch} from "react-redux";
import {BottomTabParamList, FeedParamList} from "../../navigation/types";
import colors from "../../constants/Colors";
import SendButton from "./components/SendButton";
import Input from "./components/Input";
import {addNewAlbum} from "../../redux/feed";

type Props = {
    navigation: CompositeNavigationProp<
        BottomTabNavigationProp<BottomTabParamList, 'Feed'>,
        StackNavigationProp<FeedParamList>
        >,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 21,
    backgroundColor: colors.white,
  },
});

const AddAlbum = ({navigation}: Props) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const dispatch = useDispatch();

  const sendPressed = () => {
    dispatch(addNewAlbum({title}));
    navigation.goBack();
  };

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <React.Fragment>
          {
            description !== ""
            && title !== ""
            && <SendButton goBack={sendPressed} />
          }
        </React.Fragment>

      ),
    });
  }, [title, description]);

  return (
    <View style={styles.container}>
      <Input
        title={"Title"}
        value={title}
        placeholder={"Input title"}
        setValue={setTitle} />
      <Input
        title={"Description"}
        value={description}
        placeholder={"Input description"}
        setValue={setDescription} />
    </View>
  );
};

export default AddAlbum;
