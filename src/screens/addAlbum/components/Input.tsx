import React from "react";
import {
  View, Text, StyleSheet, TextInput,
} from "react-native";
import colors from "../../../constants/Colors";

type Props = {
    title: string,
    value: string,
    placeholder: string,
    setValue: (text: string) => void,
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    width: "100%",
    height: 112,
    padding: 16,
    justifyContent: "space-between",
  },
  textInput: {
    height: 48,
    width: "100%",
    borderWidth: 1,
    borderColor: "#DBDBDB",
    borderRadius: 4,
    paddingHorizontal: 8,
  },
  title: {
    fontSize: 14,
    fontFamily: "Ubuntu-Regular",
    color: colors.grey,
  },
});

const Input = ({
  title, value, setValue, placeholder,
}: Props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <TextInput
        value={value}
        style={styles.textInput}
        placeholder={placeholder}
        onChangeText={setValue} />
    </View>
  );
};

export default React.memo(Input);
