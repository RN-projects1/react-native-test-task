import React from "react";
import {Text, TouchableOpacity} from "react-native";
import colors from "../../../constants/Colors";

type Props = {
    goBack: () => void
}
const SendButton = ({goBack}: Props) => (
  <TouchableOpacity
    style={{paddingRight: 16}}
    onPress={goBack}>
    <Text style={{color: colors.black, fontSize: 16}}>
            Send
    </Text>
  </TouchableOpacity>
);

export default React.memo(SendButton);
