import * as React from "react";
import {StyleSheet, View} from "react-native";
import {CompositeNavigationProp} from "@react-navigation/native";

import {BottomTabNavigationProp} from "@react-navigation/bottom-tabs";
import {StackNavigationProp} from "@react-navigation/stack";
import {faqData, faqItem} from "../../constants/Faq";
import TopicItem from "./components/TopicItem";
import {BottomTabParamList, FAQParamList} from "../../navigation/types";
import colors from "../../constants/Colors";

type Props = {
    navigation: CompositeNavigationProp<
        BottomTabNavigationProp<BottomTabParamList, 'FAQ'>,
        StackNavigationProp<FAQParamList>
        >
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});

const Faq = (props: Props) => {
  const navigateToQuestion = (item: faqItem) => {
    props.navigation.navigate("Question", {data: item});
  };
  return (
    <View style={styles.container}>
      {faqData.map((item) => (<TopicItem
        key={item.id}
        topic={item.topic}
        onPress={() => navigateToQuestion(item)} />))}
    </View>
  );
};

export default Faq;
