import React from "react";
import {
  View, TouchableOpacity, Text, StyleSheet, Image,
} from "react-native";
import colors from "../../../constants/Colors";

type Props = {
    topic: string,
    onPress: () => void,
};

const arrowRightImage = require("../../../../assets/images/arrowRight.png");

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 64,
    paddingLeft: 16,
  },
  item: {
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingRight: 16,
    borderBottomColor: colors.grey,
    borderBottomWidth: 1,

  },
  icon: {
    width: 12,
    height: 12,
  },
  text: {
    fontFamily: "Ubuntu-Regular",
    color: colors.black,
    fontSize: 14,
  },
});

const TopicItem = (props: Props) => (
  <View style={styles.container}>
    <TouchableOpacity
      style={styles.item}
      onPress={props.onPress}>
      <Text style={styles.text}>{props.topic}</Text>
      <Image
        source={arrowRightImage}
        style={styles.icon}
        resizeMode={"contain"} />
    </TouchableOpacity>
  </View>
);

export default TopicItem;
