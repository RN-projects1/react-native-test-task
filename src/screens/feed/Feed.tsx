import * as React from "react";
import {StyleSheet, View} from "react-native";

import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {CompositeNavigationProp} from "@react-navigation/native";
import {BottomTabNavigationProp} from "@react-navigation/bottom-tabs";
import {StackNavigationProp} from "@react-navigation/stack";
import {AppState} from "../../redux/store";
import {Album} from "../../redux/types";
import {deleteAlbum, getAlbums, getPosts} from "../../redux/feed";
import TabSwitcher from "./components/TabSwitcher";
import colors from "../../constants/Colors";
import AlbumTab from "./components/albums/AlbumsTab";
import {BottomTabParamList, FeedParamList} from "../../navigation/types";
import PostsTab from "./components/posts/PostsTab";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: colors.white,
  },
});

type Props = {
    getPosts: () => void,
    navigation: CompositeNavigationProp<
        BottomTabNavigationProp<BottomTabParamList, 'Feed'>,
        StackNavigationProp<FeedParamList>
        >
}

const Feed = (props: Props) => {
  const [activeTab, setActiveTab] = useState("Albums");

  const albums = useSelector((state: AppState) => state.feed.albums);
  const isLoading = useSelector((state: AppState) => state.feed.isLoading);
  const posts = useSelector((state: AppState) => state.feed.posts);
  const isEndPosts = useSelector((state: AppState) => state.feed.isEndPosts);
  const isEndAlbums = useSelector((state: AppState) => state.feed.isEndAlbums);

  const dispatch = useDispatch();

  const onDeleteAlbum = (id: string) => dispatch(deleteAlbum(id));
  const onAddAlbumPressed = () => props.navigation.navigate("AddAlbum");
  const openAlbum = (album: Album) => props.navigation.navigate("AlbumPhotos", {album});
  const onTabPressed = (index: string) => setActiveTab(index);

  const getData = (active: string) => {
    switch (active) {
      case "Albums":
        if (albums.length === 0) {
          dispatch(getAlbums());
        }
        break;
      case "Posts":
        if (posts.length === 0) {
          dispatch(getPosts());
        }
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    getData(activeTab);
  }, [activeTab]);

  return (
    <View style={styles.container}>
      <TabSwitcher
        values={["Albums", "Posts"]}
        active={activeTab}
        onPress={onTabPressed} />
      {
        activeTab === "Albums" && (
          <AlbumTab
            isEndAlbums={isEndAlbums}
            albums={albums}
            isLoading={isLoading}
            getNewAlbums={() => dispatch(getAlbums())}
            onDeleteAlbum={onDeleteAlbum}
            addAlbumPressed={onAddAlbumPressed}
            openAlbum={openAlbum} />
        )
      }
      {
        activeTab === "Posts" && (
          <PostsTab
            posts={posts}
            isLoading={isLoading}
            getNewPosts={() => dispatch(getPosts())}
            isEndPosts={isEndPosts} />
        )
      }
    </View>
  );
};

export default Feed;
