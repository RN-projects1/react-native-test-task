import React from "react";
import {View} from "react-native";
import colors from "../../../constants/Colors";

type Props = {
    width: number,
    height: number,
    marginTop?: number,
}
const PreloaderItem = ({width, height, marginTop = 0} : Props) => (
  <View
    style={{
      width,
      height,
      marginTop,
      borderRadius: 4,
      backgroundColor: colors.greyBorder,
    }} />
);

export default React.memo(PreloaderItem);
