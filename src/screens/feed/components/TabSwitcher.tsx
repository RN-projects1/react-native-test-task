import React from "react";
import {
  View, Text, StyleSheet, TouchableOpacity,
} from "react-native";
import colors from "../../../constants/Colors";

type Props = {
    values: string [];
    active: string;
    onPress: (index: string) => void;
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 44,
    flexDirection: "row",
    alignItems: "center",
  },
  item: {
    flex: 1,
    height: 44,
    alignItems: "center",
    justifyContent: "center",
  },
  activeItem: {
    borderBottomWidth: 3,
    borderBottomColor: colors.black,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
  },
  itemText: {
    fontSize: 16,
    fontWeight: "500",
    color: colors.grey,
  },
  activeItemText: {
    color: colors.black,
    fontFamily: "Ubuntu-Medium",
  },
});

const TabSwitcher = ({values, active, onPress}: Props) => (
  <View style={styles.container}>
    {
      values.map((value, index) => (
        <TouchableOpacity
          key={`${value}_${index}`}
          onPress={() => onPress(value)}
          style={[
            styles.item,
            active === value && styles.activeItem,
          ]}>
          <Text
            style={[
              styles.itemText,
              active === value && styles.activeItemText,
            ]}>
            {value}
          </Text>
        </TouchableOpacity>
      ))
    }
  </View>
);

export default React.memo(TabSwitcher);
