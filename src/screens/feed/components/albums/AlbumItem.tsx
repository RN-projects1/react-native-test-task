import React from "react";
import {
  View, Text, StyleSheet, Image, TouchableWithoutFeedback,
} from "react-native";
import {Album} from "../../../../redux/types";
import colors from "../../../../constants/Colors";
import PreloaderItem from "../PreloaderItem";

type Props = {
    data: Album,
    onPress: () => void
}

const styles = StyleSheet.create({
  container: {
    height: 128,
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    marginLeft: 16,
    justifyContent: "flex-start",
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderBottomColor: colors.greyBorder,
  },
  image: {
    width: 96,
    height: 96,
    borderRadius: 4,
  },
  textContainer: {
    marginLeft: 16,
    height: "100%",
    width: 230,
  },
  name: {
    fontSize: 10,
    fontFamily: "Ubuntu-Regular",
    color: colors.grey,
  },
  title: {
    fontSize: 16,
    fontFamily: "Ubuntu-Medium",
    marginTop: 8,
    color: colors.black,
    fontWeight: "500",
  },
});

const AlbumItem = ({data, onPress}: Props) => (
  <TouchableWithoutFeedback onPress={onPress}>
    <View style={styles.container}>
      {
        data.photos?.data[0]?.thumbnailUrl ? (
          <Image
            source={{uri: data.photos.data[0].thumbnailUrl}}
            style={styles.image} />
        ) : (
          <PreloaderItem
            width={96}
            height={96} />
        )
      }
      <View style={styles.textContainer}>
        <Text style={styles.name}>{data.user.name.toUpperCase()}</Text>
        <Text style={styles.title}>{data.title}</Text>
      </View>
    </View>
  </TouchableWithoutFeedback>
);

export default React.memo(AlbumItem);
