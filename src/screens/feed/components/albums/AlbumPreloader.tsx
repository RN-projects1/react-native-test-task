import React from "react";
import {View, StyleSheet} from "react-native";
import PreloaderItem from "../PreloaderItem";

const styles = StyleSheet.create(({
  container: {
    width: "100%",
    height: 128 * 3,
  },
  item: {
    width: "100%",
    height: 128,
    padding: 16,
    flexDirection: "row",
  },
  columnItems: {
    marginLeft: 16,
  },
}));

const renderItem = () => (
  <View style={styles.item}>
    <PreloaderItem
      width={96}
      height={96} />
    <View style={styles.columnItems}>
      <PreloaderItem
        width={120}
        height={10} />
      <PreloaderItem
        width={254}
        height={16}
        marginTop={12} />
      <PreloaderItem
        width={60}
        height={10}
        marginTop={14} />
    </View>
  </View>
);
const AlbumPreloader = () => (
  <View style={styles.container}>
    {renderItem()}
    {renderItem()}
    {renderItem()}
  </View>
);

export default React.memo(AlbumPreloader);
