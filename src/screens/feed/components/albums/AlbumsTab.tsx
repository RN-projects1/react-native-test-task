import React, {useState} from "react";
import {
  View, StyleSheet, Image,
  ActivityIndicator, ScrollView,
} from "react-native";
import Swipeout from "react-native-swipeout";
import {Album} from "../../../../redux/types";
import AlbumItem from "./AlbumItem";
import AlbumPreloader from "./AlbumPreloader";
import colors from "../../../../constants/Colors";
import DeleteModal from "../../../../modals/Delete";
import Button from "../../../../components/Button";
import {isScrollToEndOfList} from "../../helper";

type Props = {
    albums: Album [];
    getNewAlbums: () => void;
    onDeleteAlbum: (id: string) => void;
    addAlbumPressed: () => void;
    openAlbum: (data: Album) => void;
    isLoading: boolean;
    isEndAlbums: boolean;
}

const deleteImage = require("../../../../../assets/images/delete.png");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  itemsContainer: {
    width: "100%",
    height: "100%",
  },
  indicator: {
    padding: 16,
    width: 16,
    alignSelf: "center",
  },
  deleteContainer: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  addContainer: {
    paddingHorizontal: 16,
    marginVertical: 37,
  },

});

const DeleteButton = () => (
  <View style={styles.deleteContainer}>
    <Image
      source={deleteImage}
      style={{width: 18, height: 20}} />
  </View>
);

const AlbumTab = (
  {
    albums, getNewAlbums, isLoading,
    onDeleteAlbum, addAlbumPressed, openAlbum,
    isEndAlbums,
  }: Props,
) => {
  const [isShowDeleteModal, setIsShowDeleteModal] = useState(false);
  const [deleteAlbumId, setDeleteAlbumId] = useState<string>("");
  const closeDeleteModal = () => setIsShowDeleteModal(false);
  const deleteAlbum = () => {
    if (deleteAlbumId) {
      onDeleteAlbum(deleteAlbumId);
    }
    setIsShowDeleteModal(false);
  };

  const renderSwipeOut = (data: Album []) => {
    return (
      <React.Fragment>
        {data.map((item) => {
          const swipeoutButtons = [
            {
              justifyItems: "center",
              backgroundColor: colors.red,
              onPress: () => {
                setIsShowDeleteModal(true);
                setDeleteAlbumId(item.id);
              },
              component: <DeleteButton />,
            },
          ];
          return (
            <Swipeout
              key={item.id}
              right={swipeoutButtons}
              autoClose
              backgroundColor="transparent">
              <AlbumItem
                data={item}
                onPress={() => openAlbum(item)} />
            </Swipeout>
          );
        })}
      </React.Fragment>
    );
  };

  if (albums.length === 0) {
    return <AlbumPreloader />;
  }

  return (
    <View style={styles.container}>
      <ScrollView
        onScroll={({nativeEvent}) => {
          if (isScrollToEndOfList(nativeEvent) && !isLoading) {
            getNewAlbums();
          }
        }}
        scrollEventThrottle={400}
        showsVerticalScrollIndicator={false}>
        <React.Fragment>
          <View style={styles.addContainer}>
            <Button
              text={"Add albums"}
              onPress={addAlbumPressed}
              color={colors.blue} />
          </View>
          {renderSwipeOut(albums)}
          {isLoading && !isEndAlbums && <ActivityIndicator style={styles.indicator} />}
        </React.Fragment>
      </ScrollView>
      <DeleteModal
        isVisible={isShowDeleteModal}
        onClose={closeDeleteModal}
        onDelete={deleteAlbum} />
    </View>
  );
};

export default React.memo(AlbumTab);
