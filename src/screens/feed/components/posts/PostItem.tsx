import React from "react";
import {View, Text, StyleSheet} from "react-native";
import {Post} from "../../../../redux/types";
import colors from "../../../../constants/Colors";

type Props = {
    data: Post,
}

const styles = StyleSheet.create({
  container: {
    height: "auto",
    width: "100%",
    alignItems: "center",
    marginLeft: 16,
    justifyContent: "flex-start",
  },
  textContainer: {
    paddingVertical: 16,
    minHeight: 128,
    width: "100%",
    borderBottomWidth: 1,
    borderBottomColor: colors.greyBorder,
  },
  name: {
    fontSize: 16,
    fontFamily: "Ubuntu-Regular",
    color: colors.black,
  },
  body: {
    fontSize: 10,
    color: colors.black,
    fontFamily: "Ubuntu-Regular",
    marginTop: 4,
    width: "90%",
  },
  title: {
    fontSize: 16,
    marginTop: 8,
    color: colors.black,
    fontFamily: "Ubuntu-Medium",
    width: "90%",
  },
});

const PostItem = ({data}: Props) => (
  <View style={styles.container}>
    <View style={styles.textContainer}>
      <Text style={styles.name}>{data.user.name.toUpperCase()}</Text>
      <Text style={styles.title}>{data.title}</Text>
      <Text style={styles.body}>{data.body}</Text>
    </View>
  </View>
);

export default React.memo(PostItem);
