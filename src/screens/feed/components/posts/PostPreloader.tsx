import React from "react";
import {View, StyleSheet} from "react-native";
import PreloaderItem from "../PreloaderItem";
import colors from "../../../../constants/Colors";

const styles = StyleSheet.create(({
  container: {
    width: "100%",
    height: 128 * 3,
    paddingHorizontal: 8,

  },
  item: {
    width: "100%",
    height: 128,
    paddingHorizontal: 8,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: colors.greyBorder,
  },
}));

const renderItem = () => (
  <View style={styles.item}>
    <PreloaderItem
      width={120}
      height={10} />
    <PreloaderItem
      width={254}
      height={16}
      marginTop={12} />
    <PreloaderItem
      width={366}
      height={8}
      marginTop={11} />
    <PreloaderItem
      width={366}
      height={8}
      marginTop={8} />
    <PreloaderItem
      width={366}
      height={8}
      marginTop={8} />
  </View>
);
const AlbumPreloader = () => (
  <View style={styles.container}>
    {renderItem()}
    {renderItem()}
    {renderItem()}
  </View>
);

export default React.memo(AlbumPreloader);
