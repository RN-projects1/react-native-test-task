import React from "react";
import {
  View, StyleSheet, ScrollView, ActivityIndicator,
} from "react-native";
import PostPreloader from "./PostPreloader";
import {Post} from "../../../../redux/types";
import PostItem from "./PostItem";
import {isScrollToEndOfList} from "../../helper";

type Props = {
    posts: Post [],
    isLoading: boolean,
    isEndPosts: boolean,
    getNewPosts: () => void,
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flex: 1,
    paddingBottom: 8,
  },
  indicator: {
    padding: 16,
    width: 16,
    alignSelf: "center",
  },
});

const PostsTab = ({
  posts, isLoading, getNewPosts, isEndPosts,
}: Props) => {
  if (!posts.length) {
    return <PostPreloader />;
  }
  return (
    <View style={styles.container}>
      <ScrollView
        onScroll={({nativeEvent}) => {
          if (isScrollToEndOfList(nativeEvent) && !isLoading) {
            getNewPosts();
          }
        }}
        scrollEventThrottle={400}
        showsVerticalScrollIndicator={false}>
        {
          posts.map((post) => (<PostItem
            key={post.id}
            data={post} />))
        }
        {isLoading && !isEndPosts && <ActivityIndicator style={styles.indicator} />}
      </ScrollView>
    </View>
  );
};

export default React.memo(PostsTab);
