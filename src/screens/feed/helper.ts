import {NativeScrollEvent} from "react-native";

export const isScrollToEndOfList = (
  {layoutMeasurement, contentOffset, contentSize} : NativeScrollEvent,
) => {
  const paddingToBottom = 20;
  return layoutMeasurement.height + contentOffset.y
        >= contentSize.height - paddingToBottom;
};
