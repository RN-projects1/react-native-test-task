import * as React from "react";
import {
  StyleSheet, ActivityIndicator, Text, View,
} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import colors from "../../constants/Colors";
import {getUserProfile} from "../../redux/profle";
import {AppState} from "../../redux/store";
import Item from "./components/Item";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 16,
  },
  name: {
    fontSize: 24,
    color: colors.black,
    fontFamily: "Ubuntu-Medium",
  },
});

const Profile = () => {
  const dispatch = useDispatch();
  const profile = useSelector((state: AppState) => state.profile.userProfile);
  useEffect(() => {
    // Current user has id: "1".
    dispatch(getUserProfile(1));
  }, []);

  if (!profile) {
    return (
      <View style={[styles.container, {justifyContent: "center"}]}>
        <ActivityIndicator />
      </View>
    );
  }
  const {
    email, website, company, phone, address, username,
  } = profile;
  return (
    <View style={styles.container}>
      <Text style={styles.name}>{username}</Text>
      <Item
        title={"Email"}
        text={email} />
      <Item
        title={"Website"}
        text={website} />
      <Item
        title={"Company Name"}
        text={company.name} />
      <Item
        title={"Phone"}
        text={phone} />
      <Item
        title={"Address"}
        text={address.city} />
    </View>
  );
};

export default React.memo(Profile);
