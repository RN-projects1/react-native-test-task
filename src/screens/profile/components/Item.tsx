import React from "react";
import {View, Text, StyleSheet} from "react-native";
import colors from "../../../constants/Colors";

type Props = {
    title: string,
    text: string,
}

const styles = StyleSheet.create({
  container: {
    height: 56,
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  title: {
    fontSize: 16,
    fontFamily: "Ubuntu-Regular",
    color: colors.black,
  },
  text: {
    fontSize: 16,
    fontFamily: "Ubuntu-Regular",
    color: colors.grey,
  },
});

const Item = ({title, text} : Props) => {
  if (text) {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.text}>{text}</Text>
      </View>
    );
  }
  return null;
};

export default React.memo(Item);
