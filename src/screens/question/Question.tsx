import React, {useEffect, useState} from "react";
import {
  ScrollView, StyleSheet, View,
} from "react-native";

import {CompositeNavigationProp} from "@react-navigation/native";
import {BottomTabNavigationProp} from "@react-navigation/bottom-tabs";
import {StackNavigationProp} from "@react-navigation/stack";
import {BottomTabParamList, FAQParamList} from "../../navigation/types";
import {faqItem, faqItemData} from "../../constants/Faq";
import colors from "../../constants/Colors";
import QuestionItem from "./components/QuestionItem";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});

type Props = {
    navigation: CompositeNavigationProp<
        BottomTabNavigationProp<BottomTabParamList, 'FAQ'>,
        StackNavigationProp<FAQParamList>
        >,
    route: { params: { data: faqItem }},
}

const Question = ({navigation, route}: Props) => {
  const [data, setData] = useState<faqItemData[]>([]);

  const getData = () => setData(route.params.data.data);

  useEffect(() => {
    navigation.setOptions({
      title: route.params.data.topic.toUpperCase(),
    });
  }, [navigation]);

  useEffect(() => {
    getData();
  }, []);

  return (
    <View style={styles.container}>
      <ScrollView>
        {data.map((item) => (
          <QuestionItem
            data={item}
            key={item.id} />
        ))}
      </ScrollView>
    </View>
  );
};

export default Question;
