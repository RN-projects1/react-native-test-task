import React, {useState} from "react";
import {
  View, Text, StyleSheet, Image, TouchableWithoutFeedback,
} from "react-native";
import colors from "../../../constants/Colors";

const styles = StyleSheet.create({
  container: {
    width: "100%",
    paddingLeft: 16,
    height: "auto",
  },
  item: {
    borderBottomColor: colors.grey,
    borderBottomWidth: 1,
  },
  questionContainer: {
    height: 64,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingRight: 16,
  },
  answerContainer: {
    paddingBottom: 20,
    marginRight: 16,
  },
  icon: {
    width: 12,
    height: 12,
  },
  text: {
    fontFamily: "Ubuntu-Regular",
    color: colors.black,
    fontSize: 14,
  },
  answerText: {
    fontFamily: "Ubuntu-Regular",
    color: colors.grey,
    fontSize: 14,
    width: "100%",
  },
});

const arrowUp = require("../../../../assets/images/arrowUp.png");
const arrowDown = require("../../../../assets/images/arrowDown.png");

type Props = {
    data: {
        question: string,
        answer: string,
    },
};

const QuestionItem = (props: Props) => {
  const [isActive, setIsActive] = useState(false);
  const {answer, question} = props.data;

  const toggleActive = () => setIsActive(!isActive);
  return (
    <TouchableWithoutFeedback onPress={toggleActive}>
      <View style={styles.container}>
        <View style={styles.item}>
          <View style={styles.questionContainer}>
            <Text style={styles.text}>{question}</Text>
            <Image
              source={isActive ? arrowUp : arrowDown}
              style={styles.icon}
              resizeMode={"contain"} />
          </View>
          {
            isActive && (
              <View style={styles.answerContainer}>
                <Text style={styles.answerText}>{answer}</Text>
              </View>
            )
          }
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default React.memo(QuestionItem);
